#!/usr/bin/env python
# -*- coding: utf-8 -*-

strVersion="0.2"
strProgramHeaderShort="splitgif.py v"+strVersion
strProgramHeader="---[ "+strProgramHeaderShort+" ]------------------------------------------------"
strProgramDescription="[ gif animation splitter ]"

"""

===============================================================================================
===============================================================================================
== ToDo


== ToDo
===============================================================================================
===============================================================================================
== Done


== Done
===============================================================================================
===============================================================================================
== Thoughts


== Thoughts
===============================================================================================
===============================================================================================

"""

boolAllowScreenOutput=True

if boolAllowScreenOutput: print strProgramHeader

boolDebug=True

boolDebugPrinting=False

################################################################################################
################################################################################################
## Defines


## Defines
################################################################################################
################################################################################################
## Constants


## Constants
################################################################################################
################################################################################################
## Globals


## Globals
################################################################################################
################################################################################################
## Functions

## Functions
################################################################################################
################################################################################################


if boolDebug:
  print "Importing libraries...",
import sys
import os
import subprocess
import PIL
from PIL import Image
#from PIL import ImageColor
import argparse
import time
import copy
import random
import math

sys.path.insert(0, '/projekt/_tools/xia_lib')
import xia
from xia import Fatal
from xia import Warn
from xia import Notice
from xia import Out
#
#import pdb

if boolDebug:
  print "done\n"


################################################################################################
################################################################################################
## Test code


if 1==2:
  exit(0)


## Test code
################################################################################################
################################################################################################
## Handle input parameters


objParser = argparse.ArgumentParser(description=strProgramDescription)

objParser.add_argument("-i", "--infile",
                       dest="infile",
                       help="file stub (with path) to load",
                       metavar="FILE",
                       required=True)
objParser.add_argument("-o", "--outfile",
                       dest="outfile",
                       help="filename stub to save to",
                       metavar="FILE")
#objParser.add_argument("-c", "--color-format",
#                       dest="colorformat",
#                       help="palette color format, 'ste', 'tt' or 'falcon' (default is 'ste')",
#                       metavar="STRING",
#                       choices=(COLOR_FORMAT_STE, COLOR_FORMAT_TT, COLOR_FORMAT_FALCON),
#                       required=False)
#objParser.add_argument("-b", "--bitplanes",
#                       dest="bitplanes",
#                       help="number of bitplanes of destination screen (1, 2, 4 or 8, default is 4)",
#                       metavar="NUMBER",
#                       required=False)
#objParser.add_argument("-s", "--source-bitplanes",
#                       dest="source_bitplanes",
#                       help="number of bitplanes of source, 1-8, default is based on gif's palette size",
#                       metavar="NUMBER",
#                       required=False)
#objParser.add_argument("-p", "--padding",
#                       dest="padding",
#                       help="number of pixels to pad width to (will auto-adjust to total width of even 16ths by padding zeroes) (default is 0)",
#                       metavar="NUMBER",
#                       required=False)
#objParser.add_argument("-l", "--loop",
#                       dest="loop",
#                       help="enable looping (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)
#objParser.add_argument("-d", "--doublebuffer",
#                       dest="doublebuffer",
#                       help="double-buffer mode (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)
#objParser.add_argument("-bk", "--background",
#                       dest="background",
#                       help="background mode enabled (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)
#objParser.add_argument("-n", "--info",
#                       dest="info",
#                       help="info mode (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)
#objParser.add_argument("-nx", "--info-extra",
#                       dest="infoextra",
#                       help="info-extra mode (warning, can output dozens of MB of text) (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)
#objParser.add_argument("-db", "--debugmode",
#                       dest="debugmode",
#                       help="debug mode (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)
objParser.add_argument("-v", "--verbose",
                       dest="verbose",
                       help="shows additional information (default is off)",
                       action="store_const",
                       const=True,
                       required=False,
                       default=False)
#objParser.add_argument("-q", "--quiet",
#                       dest="quiet",
#                       help="no output during processing, except errors (default is off)",
#                       action="store_const",
#                       const=True,
#                       required=False,
#                       default=False)


# Parameter defaults
boolInFileGiven=False
strInFile=""
boolOutFileGiven=False
strOutFile=""
#boolDontUnpackGif=False
#boolUnpackGif=True
#boolDoubleBuffer=False
#boolLoopMode=False
#boolInfoMode=False
#boolInfoExtraMode=False
boolVerboseMode=False
#boolQuietMode=False
#strColorFormat=COLOR_FORMAT_STE
#numScreenBitplanes=4
#boolSourceBitplanes=False
#numSourceBitplanes=0
#boolPaddingGiven=False
#boolForcedPadding=False
#numPadding=0
#boolEnableBackgroundMode=False
#boolDebugMode=False

numArgs=0
Args=vars(objParser.parse_args())

for key in Args:
  numArgs+=1
  strValue=str(Args[key])
  #print str(key)+" = "+str(strValue)
  if key=="infile" and strValue!="None":
    boolInFileGiven=True
    strInFile=strValue
  elif key=="outfile" and strValue!="None":
    boolOutFileGiven=True
    strOutFile=strValue
#  elif key=="colorformat" and strValue!="None":
#    strColorFormat=strValue
#  elif key=="bitplanes" and strValue!="None":
#    numScreenBitplanes=int(strValue)
#  elif key=="source_bitplanes" and strValue!="None":
#    boolSourceBitplanes=True
#    numSourceBitplanes=int(strValue)
#  elif key=="padding" and strValue!="None":
#    boolPaddingGiven=True
#    numPadding=int(strValue)
#  elif key=="loop" and strValue!="None":
#    boolLoopMode=xia.StrToBool(strValue)
#  elif key=="doublebuffer" and strValue!="None":
#    boolDoubleBuffer=xia.StrToBool(strValue)
#  elif key=="background" and strValue!="None":
#    boolEnableBackgroundMode=xia.StrToBool(strValue)
#  elif key=="info" and strValue!="None":
#    boolInfoMode=xia.StrToBool(strValue)
#  elif key=="infoextra" and strValue!="None":
#    boolInfoMode=xia.StrToBool(strValue)
#    boolInfoExtraMode=xia.StrToBool(strValue)
#  elif key=="debugmode" and strValue!="None":
#    boolDebugMode=xia.StrToBool(strValue)
  elif key=="verbose" and strValue!="None":
    boolVerboseMode=xia.StrToBool(strValue)
#  elif key=="quiet" and strValue!="None":
#    boolQuietMode=xia.StrToBool(strValue)

# No args = print usage and exit
if numArgs==0:
  objParser.print_help()
  exit(0)


# Test infile
numResult=xia.CheckFileExistsAndReadable(strInFile)
if numResult!=0:
  if numResult&xia.FILE_EXISTS_FAIL:
    Fatal("Can't find file \""+strInFile+"\"!")
  if numResult&xia.FILE_READABLE_FAIL:
    Fatal("Can't read file \""+strInFile+"\"!")

## Adjust combinations of verbose and quiet mode
#if (boolVerboseMode & boolQuietMode):
#  Out("Notice: Both verbose and quiet mode sent as parameters, verbose mode chosen.\n\n")
#  boolQuietMode=False


################################################################################################
## Test that input is a valid gif


Out("Reading input file \""+strInFile+"\"... ")
arrInFile=xia.GetBinaryFile(strInFile)
try:
  imgIn=Image.open(strInFile)
except IOError:
  Fatal("\""+strInFile+"\" isn't a valid image file!")
Out("done.\n")

strImageFileFormat=imgIn.format
if strImageFileFormat.lower()!="gif":
  Fatal("\""+strInFile+"\" isn't a GIF file!")
strImageFileMode=imgIn.mode
if strImageFileMode.lower()!="p":
  Fatal("\""+strInFile+"\" isn't a palette-based file!")


## Test that input is a valid gif
################################################################################################

################################################################################################
## Extract image info


arrImageDimensions=imgIn.size
numImageWidth=arrImageDimensions[0]
numImageHeight=arrImageDimensions[1]

if boolVerboseMode:
  Out("\nExtracting palette... ")
  xia.TimingStart()

numColorTableByte=ord(arrInFile[10])
#print str(numColorTableByte)+"  "+str(type(numColorTableByte))
numImageBitplanes=(numColorTableByte&0b111)+1
numPaletteSize=pow(2, numImageBitplanes)

numPalFileOffset=13
arrGifPal=[]
numColorsInPal=0
for i in range(numPaletteSize):
  thisCol=arrInFile[numPalFileOffset:numPalFileOffset+3]
  arrGifPal.extend((thisCol[0], thisCol[1], thisCol[2]))
  numPalFileOffset+=3
  numColorsInPal+=1

arrPalette=imgIn.getpalette()
arrPalette=arrPalette[:numColorsInPal*3]
while (len(arrPalette))<768:
  arrPalette.append(0)

if boolVerboseMode:
  numTime=round(xia.GetTimingEnd(),3)
  Out("Done! Read "+str(numColorsInPal)+" colors in "+str(numTime)+" seconds)\n")


## Extract image info
################################################################################################
## Extract frames


arrGifFrames=[]
if boolVerboseMode:
  Out("Reading gif frames... ")
  xia.TimingStart()

objFrame=Image.open(strInFile)
numFrames=0
while objFrame:
  arrGifFrames.append(copy.deepcopy(objFrame))
  numFrames+=1
  try:
    objFrame.seek(numFrames)
  except EOFError:
    break;

if boolVerboseMode:
  numTime=round(xia.GetTimingEnd(),3)
  Out("Done! Read "+str(numFrames)+" frames in "+str(numTime)+" seconds)\n")


## Extract frames
################################################################################################
## Save frames


if boolVerboseMode:
  Out("\nSaving frames...\n")
  xia.TimingStart()

numFrame=0
for numFrame in range(len(arrGifFrames)):
  strFilename=strOutFile+"_"+str(numFrame).zfill(4)+".gif"
  imgOut=Image.new("P", (numImageWidth, numImageHeight))
  imgOut.putdata(arrGifFrames[numFrame].getdata())
  imgOut.save(strFilename, "gif", palette=arrPalette)
  Out("  Saved \""+strFilename+"\"\n")

if boolVerboseMode:
  numTime=round(xia.GetTimingEnd(),3)
  Out("Done! Saved "+str(numFrames)+" frames in "+str(numTime)+" seconds)\n")


## Save frames
################################################################################################

















