## splitgif

A script to split an animated .gif into multiple single-image .gifs


### Requirements

* Python v2.7.x

* Pillow (PIL fork)

    pip install pillow

* xia.py

    https://bitbucket.org/peralmered/xia_lib/src/master/


### Usage

    python splitgif.py -i *input_file* -o *output_stub*


### Example

Take "kitten.gif", extract animation frames, save frames as "meow_0000.gif", "meow_0001.gif" etc:

    python splitgif.py -i kitten.gif -o meow.gif


### All switches

#### -i [or --infile] *input_file* **required**

Input .gif file

#### -o [or --outfile] *output_stub*

Output filename stub; after this, splitgif will add "_*nnnn*.gif", where *nnnn* is a number from 0000 and up

#### -v [or --verbose]

Extra info during execution


